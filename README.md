# Analog clock

Simple analog clock 🤷

## Run locally

`yarn install && yarn start`

## Build

`yarn build`

___

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
